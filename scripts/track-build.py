from __future__ import print_function
import pickle
import os.path
import argparse
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from slugify import slugify

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1tFYyS4M71WxsbCqhUXC6HDa6bRTOQPLBcXsB0hIIpWQ'
HEADER_RANGE_NAME = 'Builds!A1:M1'

def get_service():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=8888)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return build('sheets', 'v4', credentials=creds)

def get_all_values(service):
  all_values = service.spreadsheets().values().get(
      spreadsheetId=SAMPLE_SPREADSHEET_ID, range="A1:Z9999").execute()
  return all_values['values']

def get_row_count(service):
  result = get_all_values(service)
  row_count = len(result)
  return row_count

def update_row(service, values, row):
  insert_position = "A{}".format(row)
  print("Insert Position: {}".format(insert_position))

  body = {
      'values': [values]
  }
  result = service.spreadsheets().values().update(
      spreadsheetId=SAMPLE_SPREADSHEET_ID, range=insert_position,
      valueInputOption='RAW', body=body).execute()

  print('{0} cells updated.'.format(result.get('updatedCells')))

def get_rows_match(service, values):
  all_values = get_all_values(service)
  result = []
  # import ipdb; ipdb.set_trace()
  for row_values in all_values:
    match = True
    for row_idx, value in enumerate(values):
      if value and value != row_values[row_idx]:
        match = False
        break
    if match:
      result.append(row_values)
  return result

def get_first_match(service, values):
  all_values = get_all_values(service)
  for row_idx, row_values in enumerate(all_values):
    match = True
    for col_idx, value in enumerate(values):
      if value and value != row_values[col_idx]:
        match = False
        break
    if match:
      return row_idx
  return None


def get_header(service):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range=HEADER_RANGE_NAME).execute()
    header = result['values'][0]
    return header

def parse_args(service):
    CMD_ARG = 'command'
    parser = argparse.ArgumentParser()
    parser.add_argument(CMD_ARG, choices=['put', 'get'])

    header = get_header(service)
    for col_name in header:
      parser.add_argument("--{}".format(slugify(col_name)), type=str)

    args = parser.parse_args()
    command = args.command

    values_dict = vars(args)
    del values_dict[CMD_ARG]
    values = list(values_dict.values())

    return command, values

def get_message(service, values):
    header = get_header(service)
    rows = [header]
    rows += get_rows_match(service, values)
    return rows

def main():
    service = get_service()

    command, values = parse_args(service)
    print("Command: {}".format(command))
    print("Arguments: {}".format(values))

    if command == 'put':
      row = get_first_match(service, values)
      if row is None:
        row = get_row_count(service)
      print("Row Count: {}".format(row))
      update_row(service, values, row + 1)
    elif command == 'get':
      print(get_message(service, values))

if __name__ == '__main__':
    main()